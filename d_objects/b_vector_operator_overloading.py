import math

class Vector2D:
    @property
    def x(self):
        return self._x

    @property
    def y(self):
        return self._y

    @property
    def length(self):
        return math.sqrt(self._x**2 + self._y**2)

    def __init__(self, x=0.0, y=0.0):
        # we say with the leading underscore that these are not to be touched from outside
        self._x = x
        self._y = y
    
    # for operator +
    def __add__(self, other:"Vector2D"):
        return Vector2D(self._x + other._x, self._y + other._y)
    
    # for operator -
    def __sub__(self, other:"Vector2D"):
        return Vector2D(self._x - other._x, self._y - other._y)
    
    # for operator >
    def __gt__(self, other:"Vector2D"):
        return self.length > other.length
    
    # for operator <
    def __lt__(self, other:"Vector2D"):
        return self.length < other.length

    # when converting to string
    def __str__(self):
        return f"[{self._x:+.3f}; {self._y:+3f}]"

def main():
    a = Vector2D(1.0, 3.0)
    print(f"a={a}    |a|={a.length}")
    print(f"a.x={a.x}, a.y={a.y}")

    b = Vector2D(4.0, 2.0)
    print(f"b={b}    |b|={b.length}")
    print(f"b.x={b.x}, b.y={b.y}")

    # c = a.__add__(b) is equal to c = a + b
    print(f"{a} + {b} = {a + b}")
    print(f"{a} - {b} = {a - b}")
    print(f"{a} > {b} = {a > b}")
    print(f"{a} < {b} = {a < b}")

##### Main program #####
if __name__ == "__main__":
    main()