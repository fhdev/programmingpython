class Person:
    def __init__(self, first, last):
        self.__first = first
        self.__last = last

    def __str__(self):
        return f"I'm {self.__first} {self.__last}."
    
def main():
    james = Person("James", "Bond")
    print(james)
    try:
        print(james.__first)
    except AttributeError:
        print(f"james.__first does not exit")
    # the interpreter will change __first to _Person__first
    print(james._Person__first)

##### Main program #####
if __name__ == "__main__":
    main()