class Animal:
    def __init__(self, name, group, number_of_legs):
        self.name = name
        self.group = group
        self.number_of_legs = number_of_legs

    def sayHello(self):
        print(f"I'm {self.name} and I say hello!")

    def walk(self):
        print(f"{self.name} from group {self.group} walks on {self.number_of_legs} legs.")

class Dog(Animal):
    def __init__(self, name, color, type):
        super().__init__(name, "mammal", 4)
        self.color = color
        self.type = type

    def woof(self):
        print(f"{self.name} who is a {self.type} and {self.color}-colored says woof.")

    def sayHello(self):
        super().sayHello()
        print(f"By the way, I'm a {self.color} {self.type}.")

def main():
    print("------------------------")
    spider = Animal("George", "invertebrates", 8)
    spider.walk()
    spider.sayHello()
    
    print("------------------------")
    dog = Dog("John", "brown", "retriever")
    dog.walk()
    dog.sayHello()
    dog.woof()

##### Main program #####
if __name__ == "__main__":
    main()