import copy
import math

##### non object-oriented solution #####
def area(circle):
    return math.pi * circle["r"] ** 2

def is_intersecting(circle1, circle2):
    distance = math.sqrt((circle1["x"] - circle2["x"])**2 + (circle1["y"] - circle2["y"])**2)
    return distance < (circle1["r"] + circle2["r"])

def main_wo_class():
    c1 = {
        "x": 2.0,
        "y": 2.0,
        "r": 2.0
    }
    print(f"c1={c1}; a1={area(c1)}")

    c2 = {
        "x": 10.0,
        "y": 10.0,
        "r": 4.0
    }
    print(f"c2={c2}; a2={area(c2)}")

    c3 = {
        "x": 2.0,
        "y": 0.0,
        "r": 3.0 
    }
    print(f"c3={c3}; a3={area(c3)}")
    
    print(f"c1 intersects with c2: {is_intersecting(c1, c2)}")
    print(f"c1 intersects with c3: {is_intersecting(c1, c3)}")

##### object-oriented solution #####
class Circle2D:
    # static variables = every instance has it in common
    PI               = math.pi
    INSTANCE_COUNTER = 0

    # constructor
    def __init__(self, x=0.0, y=0.0, r=0.0):
        # assign a value (e.g. None) to all possible instance varaibles
        self.x = x
        self.y = y
        self.r = r
        self.INSTANCE_COUNTER += 1

    # automatic string conversion function
    def __str__(self):
        return f"Circle2D(x={self.x},y={self.y},r={self.r})"

    # calculate area with this method
    def area(self):
        return self.PI * self.r ** 2.0
    
    # check if this circle intersects with the other circle
    def is_intersecting(self, other: "Circle2D"):
        distance = math.sqrt((self.x - other.x)**2 + (self.y - other.y)**2)
        return distance < (self.r + other.r)
    
    def copy(self):
        return Circle2D(self.x, self.y, self.r)

def main():
    print(f"Number of circles = {Circle2D.INSTANCE_COUNTER}")

    C1 = Circle2D(2.0, 2.0, 2.0)
    print(F"C1={C1}; A1={C1.area()}; id(C1)=0x{id(C1):X}")

    print(f"Number of circles = {Circle2D.INSTANCE_COUNTER}")

    C2 = Circle2D(10.0, 10.0, 4.0)
    print(F"C2={C2}; A2={C2.area()}; id(C3)=0x{id(C2):X}")

    print(f"Number of circles = {Circle2D.INSTANCE_COUNTER}")

    C3 = Circle2D(2.0, 0.0, 3.0)
    print(F"C3={C3}; A3={C3.area()}; id(C3)=0x{id(C3):X}")

    print(f"Number of circles = {Circle2D.INSTANCE_COUNTER}")

    print(f"C1 intersects with C2: {C1.is_intersecting(C2)}")
    print(f"C1 intersects with C3: {C1.is_intersecting(C3)}")

    C1b = C1.copy()
    print(F"C1b={C1b}; A1b={C1b.area()}; id(C1b)=0x{id(C1b):X}")

    C1c = copy.deepcopy(C1)
    print(F"C1c={C1c}; A1c={C1c.area()}; id(C1c)=0x{id(C1c):X}")

##### Main program #####
if __name__ == "__main__":
    main_wo_class()
    print("---------------------------")
    main()