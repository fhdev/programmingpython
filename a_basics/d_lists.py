import time

print("##### list elements #####")
list_1 = [1, 2, 3, 4, 5]
print(list_1)
for element in list_1:
    print(element)

print("##### extending a list #####")
fruits = ["apple", "peach", "pear"]
print(fruits)
for fruit in fruits:
    print(fruit)
fruits.append("banana")
print(fruits)
fruits.extend(["grape", "plum"])
print(fruits)

print("##### mixed-type list #####")
mixed_list = ["alma", 1, 3.141592654]
print(mixed_list)
for elem in mixed_list:
    print(elem)

print("##### list with given size #####")
list_size = 10
list_2 = [0] * list_size
print(list_2)

print("##### list slicing #####")
list_3 = list(range(0,20))
print(f"list_3 = {list_3}")
print(f"list_3[0:9:3] = {list_3[0:9:3]}")
print(f"list_3[:9:3] = {list_3[:9:3]}")
print(f"list_3[:-3] = {list_3[:-3]}")
print(f"list_3[::-1] = {list_3[::-1]}")
list_3[5] = 1111
print(f"list_3 = {list_3}")
list_3[10:13] = [2222] * 3
print(f"list_3 = {list_3}")
list_3[15:] = [3333]
print(f"list_3 = {list_3}")

print("##### list comprehension 1 #####")
directory = "directory"
files = ["file1.txt", "file2.py", "file3.jpg"]
# with conventional loop
paths = [] # path = list()
for file in files:
    path = f"{directory}/{file}"
    paths.append(path)
print(paths)
# with list comprehension
paths = [f"{directory}/{file}" for file in files]
print(paths)

print("##### list comprehension 2 #####")
firstNames = ["Anna", "Béla", "Dávid", "Alex", "István", "Aladár"]
# with conventional loop
names_with_a = []
for name in firstNames:
    if name.lower().startswith("a"):
        names_with_a.append(name)
print(names_with_a)
# list comprehension
names_with_a = [name for name in firstNames if name.lower().startswith("a")]
print(names_with_a)

print("##### list comprehension 3 #####")
numbers = [1, 2, 9, 12, 23]
numbers_times_two = [2 * number for number in numbers]
print(numbers_times_two)

print("##### loop over elements with index #####")
firstNames = ["Anna", "Béla", "Anna", "Dávid", "Alex", "István", "Dávid", "Aladár"]
print(firstNames)
for i, name in enumerate(firstNames):
    # index function returns the index of the first ocurrence
    indexFirst = firstNames.index(name)
    indexLast = len(firstNames) - 1 - firstNames[::-1].index(name)
    print(f"index={i}, indexFirst={indexFirst}, indexLast={indexLast}, name={name}")

print("##### loop over multiple lists at once #####")
# Uncomment following line to have equal list sizes
#firstNames = ["Anna",   "Béla", "Dávid",   "Alex",   "István", "Aladár"]
firstNames = ["Anna",   "Béla", "Dávid",   "Alex",   "István", "Aladár", "Ferenc"]
lastNames  = ["Kovács", "Tóth", "Hegedüs", "Tóbiás", "Szőke",  "Ács"]
print(firstNames)
print(lastNames)
try:
    for i in range(0, len(firstNames)):
        print(f"{lastNames[i]} {firstNames[i]}")
except IndexError:
    print("IndexError was thrown.")
print("----------")
try:
    for i, fn in enumerate(firstNames):
        print(f"{lastNames[i]} {fn}")
except IndexError:
    print("IndexError was thrown.")
print("----------")
# zip will go to the last element of the shorter list
for ln, fn in zip(lastNames, firstNames):
    print(f"{ln} {fn}")

print("##### double list comprehension #####")
firstNames = ["Anna",   "Béla", "Dávid"]
lastNames  = ["Kovács", "Tóth"]
print(firstNames)
print(lastNames)
allNames = [f"{lastName} {firstName}" for firstName in firstNames for lastName in lastNames]
print(allNames)
allNamesList = [[lastName, firstName] for firstName in firstNames if not firstName.lower().startswith("b") for lastName in lastNames if lastName.lower().startswith("k")]
print(allNamesList)
