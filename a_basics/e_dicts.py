print("##### dict fields #####")
dict_1 = {
    "name": "Peter",
    "job": "software developer",
    "legs": 2
}
print(dict_1)
print(f"name = {dict_1['name']}")
for key in dict_1:
    print(f"{key} is {dict_1[key]}")

print("##### update dict #####")
dict_2 = {
    "eyes": "blue"
}
print(dict_2)
dict_1.update(dict_2)
print(dict_1)

print("##### dict comprehension: create dict from list #####")
keys = ["alma", "korte", "szilva"]
# create a dictionary that contains strings and their length
dict_3 = {key: len(key) for key in keys}
print(dict_3)

print("##### dict constructor and add new field #####")
john = dict(name = "John", job = "architect", legs = 1)
print(john)
john["eyes"] = "green"
print(john)

print("##### dict is a reference #####")
jacob = john
print("id(john)=", id(john))
print("id(jacob)=", id(jacob))
jacob["salary"] = 100.0 # add key
print(john)
print(jacob)
jacob = john.copy()
print("id(john)=", id(john))
print("id(jacob)=", id(jacob))
jacob.pop("salary") # remove key
print(john)
print(jacob)



