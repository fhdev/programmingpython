int_var   = 32
float_var = 3.141592654
bool_var  = True
str_var   = "some text"

print("##### message_1 #####")
message_1 = f"int_var is '{int_var}',\nfloat_var is '{float_var}',\nbool_var is '{bool_var}',\nand str_var is '{str_var}'"
print(message_1)

print("##### message_2 #####")
message_2 = f"int_var is '{int_var:05d}',\nfloat_var is '{float_var:.5f}',\nbool_var is '{bool_var:d}',\nand str_var is '{str_var:20}'"
print(message_2)

print("##### message_3 #####")
message_3 = f"int_var is '{int_var:+5x}',\nfloat_var is '{float_var:<10.5e}',\nbool_var is '{bool_var:g}',\nand str_var is '{str_var:>20}'"
print(message_3)

# Older alternatives (not recommended to use)
print("##### message_4 #####")
message_4 = "int_var is '%d',\nfloat_var is '%f',\nbool_var is '%s',\nand str_var is '%s'" % (int_var, float_var, bool_var, str_var)
print(message_4)

print("##### message_5 #####")
message_5 = "int_var is '{}',\nfloat_var is '{}',\nbool_var is '{}',\nand str_var is '{}'".format(int_var, float_var, bool_var, str_var)
print(message_5)