print("##### Integers #####")
integer_1 = 1
integer_2 = 2
print(integer_1)
print(integer_2)
print(integer_1 + integer_2)
type_integer_1 = type(integer_1)
print(type_integer_1.__name__)

print("##### Floating points #####")
float_1 = 1.0
float_2 = 2.0
print(float_1)
print(float_2)
print(float_1 + float_2)
type_float_1 = type(float_1)
print(type_float_1.__name__)

print("##### Complex floating points #####")
complex_1 = 1.0 + 1.0j
complex_2 = 2.0 + 2.0j
print(complex_1)
print(complex_2)
print(complex_1 + complex_2)
type_complex_1 = type(complex_1)
print(type_complex_1.__name__)

print("##### Booleans #####")
bool_1 = True
bool_2 = False
print(bool_1)
print(bool_2)
print(bool_1 & bool_2)
type_bool_1 = type(bool_1)
print(type_bool_1.__name__)

print("##### None #####")
none_1 = None
print(none_1)
type_none_1 = type(none_1)
print(type_none_1.__name__)

print("##### Strings #####")
str_1 = 'string 1 '
str_2 = "string 2 "
print(str_1)
print(str_2)
print(str_1 + str_2)
type_str_1 = type(str_1)
print(type_str_1.__name__)
str_3 = """this is
a 
multi-line
string"""
print(str_3)
str_4 = "this\nis\nalso\na multi-line\nstring"
print(str_4)
str_5 = R"but\nthis\nstring\nis\nsingle-line"
print(str_5)
