MAX_TRIES = 3

def main():
    print("Calculate resultant resistance.")
    # Make sure that user inputs S or P in MAX_TRY tries, or the program will exit
    option = ""
    cnt = 0
    while option != "s" and option != "p":
        cnt += 1
        if cnt > MAX_TRIES:
            return # from main
        option = input("Do you want to calculate a serial [S] or parallel [P] circuit? ").lower()
    # Calculate resultant resistance
    Re = 0.0
    while True:
        R = float(input("R = "))
        if R <= 0.0:
            break # from while
        if option == "s":
            Re += R
        else:
            Re += 1.0 / R
    if option == "p" and Re > 0.0:
        Re = 1.0 / Re
    # Print result
    print(f"Re = {Re:.6f}") 

##### Main program #####
if __name__ == "__main__":
    main()