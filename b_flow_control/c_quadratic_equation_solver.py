import math

ALMOST_ZERO = 1e-6 # 0.000001

def read_number(name):
    # read number as string
    number = input(f"{name}=")
    # convert string to float safely
    try:
        number = float(number)
    except ValueError:
        number = 0.0
    return number

def main():
    print("Solve the equation a.x^2+b.x+c=0")
    # Read in coefficients
    a = read_number("a")
    b = read_number("b")
    c = read_number("c")
    # Handle floating point equality correctly
    if abs(a) < ALMOST_ZERO:
        if abs(b) < ALMOST_ZERO:
            if abs(c) < ALMOST_ZERO:
                print("Identity:\n0.00000 = 0.00000")
            else:
                print(f"Contradiction:\n{c:.5f} = 0.00000")
        else:
            print(f"Linear:\nx={-c / b:.5f}")
    else:
        D = b*b - 4*a*c
        if abs(D) < ALMOST_ZERO:
            print(f"Real solution with double multiplicity:\nx12 = {-b / (2.0 * a):.5f}")
        elif D < 0.0:
            re = -b / (2.0 * a)
            im = math.sqrt(-D) / (2.0 * a)
            print(f"Complex solution:\nx1 = {re:.5f} + {im:.5f}i\nx2 = {re:.5f} - {im:.5f}i")
        else:
            x1 = (-b + math.sqrt(D)) / (2.0 * a)
            x2 = (-b - math.sqrt(D)) / (2.0 * a)
            print(f"Real solution:\nx1 = {x1:.5f}\nx2 = {x2:.5f}")

##### Main program #####
if __name__ == "__main__":
    main()