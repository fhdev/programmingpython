import math

# Status flags
SUCCESS     = 0
ERROR_INDEX = 1001
ERROR_NAN   = 2002
ERROR_MIXED = 3003

# Test data
ARRAY =  [23, 43, 21, 34, math.nan]
INDICES = [[1, 100], [100, 1], [1, 4], [4, 1], [4, 100], [100, 4], [1, 2], [2, 1]]

##### Error handling without exceptions #####
def get_element_wo_exc(numericArray, index):
    # Check for index out of range
    if index < 0 or index >= len(numericArray):
        # None can not be an element of the numericArray
        return ERROR_INDEX, None
    # Check if element is not a number
    if math.isnan(numericArray[index]):
        return ERROR_NAN, None
    return SUCCESS, numericArray[index]

def add_elements_wo_exc(numericArray, index1, index2):
    
    error_flag1, element1 = get_element_wo_exc(numericArray, index1)
    if error_flag1 != SUCCESS:
        return error_flag1, None
    error_flag2, element2 = get_element_wo_exc(numericArray, index2)
    if error_flag2 != SUCCESS:
        return error_flag2, None
    return SUCCESS, (element1 + element2)

def print_result_wo_exc(error_flag, value):
    if error_flag == ERROR_INDEX:
        print("Index out of range.")
    elif error_flag == ERROR_NAN:
        print("Element is not a number.")
    else:
        print(f"Success. {value}")

def main_wo_exc():
    for i in INDICES:
        error_flag, sum = add_elements_wo_exc(ARRAY, i[0], i[1])
        print_result_wo_exc(error_flag, sum)

##### Error handling with exceptions #####
def get_element(numericArray, index):
    # Check if element is not a number
    if math.isnan(numericArray[index]):
        raise TypeError("Element is not a number.")
    return numericArray[index]

def add_elements(numericArray, index1, index2):
    return get_element(numericArray, index1) + get_element(numericArray, index2)

def main():
    for i in INDICES:
        try:
            sum = add_elements(ARRAY, i[0], i[1])
            print(f"Success. {sum}")
        except IndexError:
            print("Index out of range.")
        except TypeError as ex:
            print(str(ex))

##### Main program #####
if __name__ == "__main__":
    main_wo_exc()
    print("-------------")
    main()