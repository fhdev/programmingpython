def main():
    n = input("n = ")
    n = int(n)
    f = 1
    for i in range(1, n + 1):
        f *= i
    print(f"f = {f}")

##### Main program #####
if __name__ == "__main__":
    main()