print("##### if #####")
x = int(input("x="))

is_five_str = "five" if x == 5 else "not five"
print(is_five_str)

if x > 0:
    print("x is positive")
else:
    print("x is zero or negative")

if x % 2 == 0: # 4
    print("x is divisible by two")
elif x % 3 == 0: # 9
    print("x is divisible by three but not by two")
elif x % 5 == 0: # 25
    print("x is divisible by five but not by two or three")
else: # 7
    print("x is not divisible by two or three or five")



print("##### for #####")

for i in range(5):
    print(i, end=" ")
print()

for i in [1, 1, 2, 3, 5]:
    print(1000 + i)

for c in "this is a string":
    print(c)



print("##### while #####")

i = 0
while(i < 10):
    print(f"i={i}")
    i += 1

j = 0
while(j > -1):
    j += 1
    if j % 3 == 0:
        continue # goes to the next check of the j > -1 condition
    if j > 10:
        break # goes after the current loop
    print(f"j={j}")



print("##### try #####")

d = {"key1": "value1"}
k = "key2"

# exceptions can flow through function boundaries
try:
    print(d[k])
except KeyError:
    print(f"dict {d} has no key {k}")

# can also be solved without exceptions in this particular case
if k in d.keys():
    print(d[k])
else:
    print(f"dict {d} has no key {k}")
