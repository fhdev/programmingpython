num1 = input("Please specify the first number: ")
print(f"num1 = {num1} type(num1).__name__ = {type(num1).__name__}")

num2 = input("Please specify the second number: ")
print(f"num2 = {num2} type(num2).__name__ = {type(num2).__name__}")

try:
    # Try to convert to a floating-point value
    num1 = float(num1)
    print(f"num1 = {num1} type(num1).__name__ = {type(num1).__name__}")
except ValueError:
    # Try to convert to a floating-point value
    print(f"The value {num1} is not a number.")
    num1 = 0.0
try:
    num2 = float(num2)
    print(f"num2 = {num2} type(num2).__name__ = {type(num2).__name__}")
except ValueError:
    print(f"The value {num2} is not a number.")
    num2 = 0.0

sum = num1 + num2
print(f"result = {sum}")
