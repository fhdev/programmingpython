def fibo(n):
    if n < 2:
        return n
    else:
        return fibo(n - 1) + fibo(n - 2)

def main():
    for i in range(20):
        print(f"{i:2d} {fibo(i):4d}")

##### Main program #####
if __name__ == "__main__":
    main()