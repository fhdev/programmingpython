def add_one(num):
    return num + 1

def shout(text):
    print(text.upper())

def whisper(text):
    print(text.lower())

def greet(func):
    func("Hello, World!")

# Functions can be returned
def create_addition_function(x):
    def add(y):
        return x + y
    
    return add

def main():
    print("##### functions are objects and can be assigned to variables #####")
    print(add_one(999))
    increment = add_one
    print(increment(999))

    print("##### functions can be passed as parameters to other functions #####")
    greet(shout)
    greet(whisper)

    print("##### functions can be returned #####")
    add_five = create_addition_function(5)
    print(add_five(555))
    add_nine = create_addition_function(9)
    print(add_nine(111))

##### Main program #####
if __name__ == "__main__":
    main()