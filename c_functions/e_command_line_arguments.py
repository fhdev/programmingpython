import sys

# python e_commnad_line_arguments.py 10 20
# 10.0 + 20.0 = 30.0
def main():
    number1 = float(sys.argv[1])
    number2 = float(sys.argv[2])
    print(f"{number1} + {number2} = {number1 + number2}")

##### Main program #####
if __name__ == "__main__":
    main()