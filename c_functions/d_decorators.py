import time

# Extend functionality 
def measure_time(func):
    # Note: use args and kwars so that any arguments that are passed to the decorated function will be passed to the 
    # original function
    def measure(*args, **kwargs):
        start = time.time()
        f = func(*args, **kwargs)
        stop = time.time()
        print(f"Elapsed time is {stop - start:.6f} s")
        return f

    return measure

# Without @ notation ############################################
def factorial_1(num):
    f = 1.0
    for i in range(1, num + 1):
        f *= float(i)
    time.sleep(0.1) # just for time measurement
    return f

factorial_1 = measure_time(factorial_1)

# With @ notation ##############################################
@measure_time
def factorial_2(num):
    f = 1.0
    for i in range(1, num + 1):
        f *= float(i)
    time.sleep(0.2) # just for time measurement
    return f

@measure_time
def div(num, den):
    time.sleep(0.3) # just for time measurement
    return num / den

def main():
    print("##### measure time with decorator #####")

    f = factorial_1(100)
    print(f)

    f = factorial_2(100)
    print(f)

    s = div(2, 10)
    print(s)

    s = div(den=2, num=10)
    print(s)

##### Main program #####
if __name__ == "__main__":
    main()