# increments a number by a specified increment
def inc(num, inc=1):
    return num + inc

# concatenate two strings
def concat(stringStart, stringEnd):
    return stringStart + stringEnd

# add all arguments
def sum_of_args(*args):
    # usage of the sum function is nicer but we want to demonstrate args here
    sum = 0.0
    for arg in args:
        sum += arg
    return sum

# calculate the sum or product of all integer arguments based on the specified method
# default method is sum
def sum_or_product_of_args(*args, **kwargs):
    method = kwargs["method"] if "method" in kwargs.keys() else "sum"
    offset = kwargs["offset"] if "offset" in kwargs.keys() else 0
    # usage of the sum function is nicer for the sum  but we want to demonstrate kwargs here
    result = 0 if method == "sum" else 1
    for arg in args:
        if method == "sum":
            result += arg
        elif method == "prod":
            result *= arg
    return result + offset

##### Main program #####
def main():
    
    # default arguments
    print("##### default arguments #####")
    print(inc(1))
    print(inc(1, 2))

    # consider calling library functions like this
    print("##### pass parameters positionally or by name #####")
    print(concat("alma", "korte"))
    # this causes a TypeError because you want to specify multiple values for same parameter
    # print(concat("alma", stringStart="korte"))
    print(concat(stringEnd="alma", stringStart="korte"))
    l = ["banan", "szilva"]
    print(concat(*l)) # positional arguments as list
    d = {"stringEnd": "banan", "stringStart": "szilva"}
    print(concat(**d)) # keyword arguments as dict

    # variable number of arguments
    print("##### add as many numbers as specified #####")
    print(sum_of_args(1, 2))
    print(sum_of_args(1, 2, 3))
    print(sum_of_args(1, 2, 3, 4))
    l = [1, 2, 3, 4, 5]
    print(sum_of_args(*l))
    
    # variable number of positional arguments + keyword arguments
    print("##### key-value arguments #####")
    print(sum_or_product_of_args(1, 2, 3, 4))
    print(sum_or_product_of_args(1, 2, 3, 4, method = "prod"))
    print(sum_or_product_of_args(1, 2, 3, 4, method = "prod", offset = 100))

##### Main program #####
if __name__ == "__main__":
    main()