from typeguard import typechecked

@typechecked
def get_element(l: list, i: int) -> int:
    print(type(l))
    return l[i]

def main():
    print(get_element([0, 1, 2], 2))
    print(get_element("alma", 2)) # will throw error if typechecked is imported

##### Main program #####
if __name__ == "__main__":
    main()