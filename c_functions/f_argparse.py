import argparse

# python e_commnad_line_arguments.py -s1 alma -s2 korte
# python e_commnad_line_arguments.py --string1 alma --string2 korte
# alma + korte = almakorte
# python e_commnad_line_arguments.py -s1 alma
# alma + szilva = almaszilva
def main():
    parser = argparse.ArgumentParser(description="string concatenation")
    parser.add_argument("-s1", "--string1", dest="string1", 
                        help="left string to concatenate", required=True)
    parser.add_argument("-s2", "--string2", dest="string2", 
                        help="right string to concatenate", default="szilva")
    args = parser.parse_args()
    print(f"{args.string1} + {args.string2} = {args.string1 + args.string2}")

##### Main program #####
if __name__ == "__main__":
    main()