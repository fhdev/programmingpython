import argparse
import os

# Root directory of this script
SCRIPT_ROOT_DIR = os.path.dirname(os.path.realpath(__file__))

def listContent(dir, recursionLevel=0, recursionLimit=None):
    # Do not go below recursionLimit 
    if (recursionLimit is not None) and (recursionLevel > recursionLimit):
        return
    # List content
    for subDirName in os.listdir(dir):
        print(f"{' ' * 4 * recursionLevel}{subDirName}")
        subDir = os.path.join(dir, subDirName)
        if os.path.isdir(subDir):
            listContent(subDir, recursionLevel + 1, recursionLimit)

##### Main program #####
def main():
    # Get arguments
    parser = argparse.ArgumentParser(description="directory content listing")
    parser.add_argument("-d", "--directory", 
                        dest="directory", 
                        help="list the content of this directory", 
                        default=SCRIPT_ROOT_DIR)
    args = parser.parse_args()
    # List content
    #listContent(args.directory)
    #listContent(R"D:\Nextcloud\Repos")
    listContent(R"D:\Nextcloud\Repos", recursionLimit=2)


if __name__ == "__main__":
    main()