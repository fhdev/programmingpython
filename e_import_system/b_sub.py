print(f"Importing or running module '{__name__}' in '{__file__}'.")

PI = 3.141592654

def add(num1, num2):
    return num1 + num2

if __name__ == "__main__":
    print(f"Running module '{__name__}' in '{__file__}' directly.")